<?php
    set_time_limit(0);
    session_start();
    if (!isset($_SESSION['login'])) {   // MENGECEK APAKAH USER SUDAH LOGIN / BELUM
        // JIKA BELUM DIA ARAHKAN KE HALAMAN LOGIN
        header("Location: ./auth");
        die();
    }

    require_once('config/koneksi.php');
    require_once('models/database.php');

    $connection = new Database($host, $user, $pass, $database);
    include "models/m_cronjob.php";
    $cronjob = new Cronjob($connection);
    $cronjob->cron_jadwal();    // MENJALANKAN PERINTAH CRON_JADWAL DI M_CRONJOB
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>Aplikasi Penugasan TVRI</title>
    <!-- Bootstrap Core CSS -->
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="assets/plugins/bootstrap/css/multi-select.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script>
        function getListPegawai(whochange) {
            var tanggal_mulai = document.getElementById("TANGGAL_MULAI").value;  
            var tanggal_selesai = document.getElementById("TANGGAL_SELESAI").value;
            if (tanggal_selesai == '') {
                document.getElementById("TANGGAL_SELESAI").value = tanggal_mulai;
            } else if (tanggal_mulai > tanggal_selesai) {
                alert('Tanggal selesai harus lebih atau sama dengan tanggal mulai');
                if (whochange == 'mulai') {
                    document.getElementById("TANGGAL_MULAI").value = document.getElementById("TANGGAL_MULAI_OLD").value;
                } else {
                    document.getElementById("TANGGAL_SELESAI").value = document.getElementById("TANGGAL_SELESAI_OLD").value;
                }
            } else {
                $.ajax({
                    url: 'views/getListPegawai.php?tanggal_mulai='+tanggal_mulai+'&tanggal_selesai='+tanggal_selesai,
                    type:'GET',
                    success: function(data){
                        var data = data.split('<content>');
                        $('#form_pilih_pegawai').html(data[1]);
                        document.getElementById("TANGGAL_MULAI_OLD").value = tanggal_mulai;
                        document.getElementById("TANGGAL_SELESAI_OLD").value = tanggal_selesai;
                    }
                });
            }
        }
        function getListPegawaiUbah(whochange) {
            var tanggal_mulai = document.getElementById("TANGGAL_MULAI").value;  
            var tanggal_selesai = document.getElementById("TANGGAL_SELESAI").value;
            if (tanggal_mulai > tanggal_selesai) {
                alert('Tanggal selesai harus lebih atau sama dengan tanggal mulai');
                if (whochange == 'mulai') {
                    document.getElementById("TANGGAL_MULAI").value = document.getElementById("TANGGAL_MULAI_OLD").value;
                } else {
                    document.getElementById("TANGGAL_SELESAI").value = document.getElementById("TANGGAL_SELESAI_OLD").value;
                }
            } else {
                var kode_jadwal = document.getElementById("KODE_JADWAL").value;
                var link = 'views/getListPegawaiUbah.php?tanggal_mulai='+tanggal_mulai+'&tanggal_selesai='+tanggal_selesai+'&kode_jadwal='+kode_jadwal;
                $.ajax({
                    url: link,
                    type:'GET',
                    success: function(data){
                        var data = data.split('<content>');
                        $('#form_pilih_pegawai').html(data[1]);
                        document.getElementById("TANGGAL_MULAI_OLD").value = tanggal_mulai;
                        document.getElementById("TANGGAL_SELESAI_OLD").value = tanggal_selesai;
                    }
                });
            }
        }
        function konfirmasiHapus(payload) {
            var result = "'"+payload+"'";
            return confirm("Anda yakin ingin menghapus data "+result+" ?");
        }
    </script>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                           
                            <!-- Light Logo icon -->
                            <img src="assets/images/tvri.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         
                         <!-- Light Logo text -->    
                         
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Selamat Datang, <?php echo (isset($_SESSION['login_user']->NAMA)) ? $_SESSION['login_user']->NAMA : ucfirst($_SESSION['login_as']); ?></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <?php
                        // MENGECEK YANG LOGIN ADMIN / PEGAWAI
                        if ($_SESSION['login_as'] == 'admin') {
                            // JIKA ADMIN MAKA DITAMPILKAN MENU ADMIN
                            include_once 'menu_admin.php';
                        } else if ($_SESSION['login_as'] == 'atasan') {
                            // JIKA ADMIN MAKA DITAMPILKAN MENU ADMIN
                            include_once 'menu_atasan.php';
                        } else {
                            // JIKA PEGAWAI MAKA DITAMPILKAN MENU PEGAWAI
                            include_once 'menu_pegawai.php';
                        }
                    ?>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo ucfirst(str_replace('_',' ', @$_GET['page']));?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active"><?php echo ucfirst(str_replace('_',' ', @$_GET['page']));?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php
                    // MENGECEK HALAMAN MANA YANG INGIN DITAMPILKAN DARI URL BERDASARKAN NILAI 'PAGE'
                    if(@$_GET['page'] == 'dashboard' || @$_GET['page'] == ''){
                        include "views/dashboard.php";
                    } else if (@$_GET['page'] == 'buat_jadwal'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'lihat_jadwal'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'lihat_jadwal_detail'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'lihat_jadwal_laporan'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'lihat_jadwal_saya'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'penugasan_upload'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'manage_pegawai'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'profile_pegawai'){
                        include "views/".$_GET['page'].".php";
                    } else if (@$_GET['page'] == 'ubah_jadwal'){
                        include "views/".$_GET['page'].".php";
                    }
                ?>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                Sistem informasi Penugasan TVRI Jawa Barat
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="assets/plugins/bootstrap/js/multi-select.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/custom.min.js"></script>
</body>

</html>
