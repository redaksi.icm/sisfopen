
<title>SENDING EMAIL (WINDOWS OTOMATIS TERTUTUP SAAT EMAIL BERHASIL DIKIRIM)</title>
<?php

// UNTUK MENGIRIM EMAIL

set_time_limit(0);
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';
require_once('../config/koneksi.php');
require_once('../models/database.php');
include "../models/m_pegawai.php";
include "../models/m_jadwal.php";

$connection = new Database($host, $user, $pass, $database);
$pgw = new Pegawai($connection);
$jwl = new Jadwal($connection);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Instantiation and passing `true` enables exceptions
$email = 'mfadliahmad10@gmail.com';
$password = 'warungcepot';
$surat = file_get_contents('template_surat.html');
$surat = str_replace('[kota]', 'Bandung', $surat);
$tanggal_surat = date("d F Y");
$surat = str_replace('[tanggal_surat]', $tanggal_surat, $surat);
$surat = str_replace('[nama_atasan]', 'Muhammad Sanif', $surat);

if (isset($_GET['KODE_JADWAL'])) {
    try {
        // KIRIM EMAIL UNTUK PRODUSER
        $getAcara = $jwl->tampil($_GET['KODE_JADWAL']);
        $acara = $getAcara->fetch_assoc();
        $mail = new PHPMailer(true);
        //Server settings
        $mail->isSMTP();
        $mail->Host = 'smtp.gmail.com';  //gmail SMTP server
        $mail->SMTPAuth = true;
        $mail->Username = $email;   //username
        $mail->Password = $password;   //password
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;                    //SMTP port
        // $mail->SMTPSecure = 'tls';
        // $mail->Port = 587;                    //SMTP port
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
    
        //Recipients
        $mail->setFrom($email, 'Pemberitahuan Penugasan');
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Pemberitahuan Penugasan Acara';
        $mail->addReplyTo($email, 'Information');
        $mail->addCC($email);
    
        $mail->addAddress($acara['EMAIL'], $acara['NAMA']);     // Add a recipient
    
        // Content
        $surat = str_replace('[nama_acara]', $acara['NAMA_ACARA'], $surat);
        $surat = str_replace('[Nama_Pegawai]', $acara['NAMA'], $surat);
        $surat = str_replace('[NIP]', $acara['PRODUSER_NIP'], $surat);
        $surat = str_replace('[Jabatan]', 'PRODUSER', $surat);
        $surat = str_replace('[nama_produser]', $acara['NAMA'], $surat);
        $surat = str_replace('[tgl]', $acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI'], $surat);
        $surat = str_replace('[Shift]', $acara['JAM'], $surat);
        $surat = str_replace('[lokasi]', $acara['LOKASI'], $surat);
        $mail->Body    = $surat;
    
        if ($mail->send()) {
            echo 'Message has been sent to produser '.$acara['EMAIL'].'<br>';
        } else {
            echo 'Fail to send message to produser '.$acara['EMAIL'].'<br>';
        }

        // KIRIM EMAIL UNTUK PEGAWAI
        $sql = "SELECT * FROM tbl_penugasan t, tbl_pegawai p WHERE t.KODE_JADWAL = ".$_GET['KODE_JADWAL']." AND t.NIP = p.NIP";
        $getListPegawai = $pgw->query($sql);
        
        while($penerima = $getListPegawai->fetch_assoc()) {
            $mail = new PHPMailer(true);
            //Server settings
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';  //gmail SMTP server
            $mail->SMTPAuth = true;
            $mail->Username = $email;   //username
            $mail->Password = $password;   //password
            $mail->SMTPSecure = 'ssl';
            $mail->Port = 465;                    //SMTP port
            // $mail->SMTPSecure = 'tls';
            // $mail->Port = 587;                    //SMTP port
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
        
            //Recipients
            $mail->setFrom($email, 'Pemberitahuan Penugasan');
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Pemberitahuan Penugasan Acara';
            $mail->addReplyTo($email, 'Information');
            $mail->addCC($email);
        
            $mail->addAddress($penerima['EMAIL'], $penerima['NAMA']);     // Add a recipient
        
            // Content
            $surat = str_replace('[nama_acara]', $acara['NAMA_ACARA'], $surat);
            $surat = str_replace('[Nama_Pegawai]', $acara['NAMA'], $surat);
            $surat = str_replace('[NIP]', $penerima['NIP'], $surat);
            $surat = str_replace('[Jabatan]', $penerima['JABATAN'], $surat);
            $surat = str_replace('[nama_produser]', $acara['NAMA'], $surat);
            $surat = str_replace('[tgl]', $acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI'], $surat);
            $surat = str_replace('[Shift]', $acara['JAM'], $surat);
            $surat = str_replace('[lokasi]', $acara['LOKASI'], $surat);
            $mail->Body = $surat;
        
            if ($mail->send()) {
                echo 'Message has been sent to '.$penerima['EMAIL'].'<br>';
            } else {
                echo 'Fail to send message to '.$penerima['EMAIL'].'<br>';
            }
        }
        // echo "<script>window.close();</script>";
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    
    }
} else {
    echo "No email";
}
?>