<?php

class Cronjob {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }

    // FUNCTION UNTUK MENGUBAH STATUS ACARA/JADWAL MENJADI SELESAI/FINISHED JIKA TANGGAL_SELESAI SUDAH TERLEWAT
    public function cron_jadwal(){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_jadwal SET STATUS = 'FINISHED' WHERE TANGGAL_SELESAI < NOW() AND STATUS = 'RUNNING'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>