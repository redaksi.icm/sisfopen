<?php
    include "models/m_pegawai.php";
    include "models/m_jadwal.php";
    $pgw = new Pegawai($connection);
    $jwl = new Jadwal($connection);
?>
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
<div class="row">
    <!-- column -->
    <div class="col-lg-12">
        <div class="card">
            <div class="card-block">
            <h4 class="card-title">Form Data Acara</h4>
            <hr>
                <?php
                    if (isset($_POST['ubah']) || isset($_POST['tambah'])) {
                        if (isset($_POST['ubah'])) {    // JIKA BUTTON UBAH DITEKAN
                            $data = $jwl->ubah($_POST); // MEMANGGIL FUNCTION UBAH DARI M_JADWAL
                            $message = 'Diubah';
                        } else if (isset($_POST['tambah'])) {   // JIKA BUTTON TAMBAH DITEKAN
                            $_POST['LOKASI'] = $_POST['LOKASI'].' ('.$_POST['LOKASI2'].')';
                            $data = $jwl->tambah($_POST);   // MEMANGGIL FUNCTION TAMBAH DARI M_JADWAL
                            $message = 'Ditambahkan';
                        }
                        if ($data) {
                            // JIKA BERHASIL MENGUBAH/MENAMBAHKAN DATA DIA MASUK KESINI
                            echo '
                            <div class="alert alert-success"> <i class="ti-user"></i> Data Berhasil '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        } else {
                            // JIKA TIDAK BERHASIL MENGUBAH/MENAMBAHKAN DATA DIA MASUK KESINI
                            echo '
                            <div class="alert alert-danger"> <i class="ti-user"></i> Data Gagal '.$message.'.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                            </div>
                            ';
                        }
                    }
                ?>
                <form acion="" method="post" class="form-material">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama-acara" class="col-md-12">NAMA ACARA</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="NAMA_ACARA" id="NAMA_ACARA" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Pilih Produser Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="PRODUSER_NIP" name="PRODUSER_NIP">
                                        <?php
                                            $produser = $pgw->tampil_filter('JABATAN', 'PRODUSER'); // MEMANGGIL FUNCTION TAMPIL_FILTER DARI M_PEGAWAI
                                            if (!$produser || @$produser->num_rows == 0) {
                                                echo '<option>Tidak Ada Pegawai Dengan Jabatan Produser</option>';
                                            } else {
                                                while ($data = $produser->fetch_object()) {
                                                    echo '<option value="'.$data->NIP.'">'.$data->NAMA.' - '.$data->NIP.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="TANGGAL_MULAI_OLD" id="TANGGAL_MULAI_OLD" value="<?php echo date("Y-m-d"); ?>"/>
                        <input type="hidden" name="TANGGAL_SELESAI_OLD" id="TANGGAL_SELESAI_OLD" value=""/>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Mulai</label>
                                <div class="col-md-12">
                                    <input type="date" value="<?php echo date("Y-m-d");?>" class="form-control form-control-line"name="TANGGAL_MULAI" id="TANGGAL_MULAI" onChange="getListPegawai('mulai');" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Tanggal Selesai</label>
                                <div class="col-md-12">
                                    <input type="date" class="form-control form-control-line"name="TANGGAL_SELESAI" id="TANGGAL_SELESAI" onChange="getListPegawai('selesai');" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for ="nama-produser"class="col-md-12">Pilih Jam Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="JAM" name="JAM">
                                        <option value="Tidak dalam shift">Tidak Dalam Shift</option>
                                        <option value="Shift 1 ( Pukul 08:00 - 12:00 )">Shift 1 ( Pukul 08:00 - 12:00 )</option>
                                        <option value="Shift 2 ( Pukul 13:00 - 17:00 )">Shift 2 ( Pukul 13:00 - 17:00 )</option>
                                        <option value="Shift 3 ( Pukul 18:00 - 21:00 )">Shift 3 ( Pukul 18:00 - 21:00 )</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Lokasi Acara</label>
                                <div class="col-md-12">
                                    <select class="custom-select col-12" id="LOKASI" name="LOKASI">
                                        <option value="DIDALAM KANTOR">DIDALAM KANTOR</option>
                                        <option value="DILUAR KANTOR">DILUAR KANTOR</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="tanggal-mulai" class="col-md-12">Keterangan Lokasi</label>
                                <div class="col-md-12">
                                    <input type="text" class="form-control form-control-line" name="LOKASI2" id="LOKASI2" placeholder="nama studio, dsb" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <br>
                            <h4 class="card-title">Form Penugasan Pegawai</h4>
                            <hr>
                        </div>
                        <div class="col-md-12" id="form_pilih_pegawai">
                            <h1>Silahkan Pilih Tanggal Mulai & Tanggal Selesai Dahulu</h1>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="submit" class="btn btn-success" name="tambah" value="BUAT JADWAL"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>        
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
