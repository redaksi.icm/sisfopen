<?php
    include "../models/m_pegawai.php";
    include "../models/m_jadwal.php";
    include "../models/m_penugasan.php";
    require_once('../config/koneksi.php');
    require_once('../models/database.php');

    $connection = new Database($host, $user, $pass, $database);
    $pgw = new Pegawai($connection);
    $jwl = new Jadwal($connection);
    $tgs = new Penugasan($connection);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>Aplikasi Penugasan TVRI</title>
    <!-- Bootstrap Core CSS -->
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/plugins/bootstrap/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="../assets/plugins/bootstrap/css/multi-select.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="../css/colors/blue.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="../https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <content>
    <div class="form-group">
        <small>
            <ul>
                <li>Pilih pegawai yang akan ditugaskan</li>
                <li>Pegawai yang tidak dapat dipilih, berarti sudah ditugaskan pada jadwal lain</li>
            </ul>
        </small>
        <input type="hidden" name="JUMLAH_PEGAWAI" value="0"/>
        <?php
            $petugas = $tgs->tampil_active($_GET['kode_jadwal']);
            $list_petugas = [];
            for($p = 0; $p < count($petugas) ; $p++) {
                $p_temp = $petugas[$p];
                array_push($list_petugas, $p_temp->NIP);
            }
        
                // MENGAMBIL DATA PEGAWAI SELAIN PRODUSER YANG SEDANG TIDAK BERTUGAS PADA TANGGAL YANG DITENTUKAN
            $jabatan = $pgw->query('SELECT JABATAN FROM tbl_pegawai WHERE JABATAN != "PRODUSER" GROUP BY JABATAN ORDER BY JABATAN ASC');
            if (!$jabatan || @$jabatan->num_rows == 0) {
                echo '<option>Tidak Ada Pegawai Selain Produser</option>';
            } else {
                $no=0;
                while ($row = $jabatan->fetch_object()) {
                        // MENGAMBIL DATA PEGAWAI SELAIN PRODUSER YANG SEDANG TIDAK BERTUGAS PADA TANGGAL YANG DITENTUKAN
                    $pegawai = $pgw->query('SELECT NIP, NAMA FROM tbl_pegawai WHERE JABATAN = "'.$row->JABATAN.'" ORDER BY NAMA ASC');
                    if (!$pegawai || @$pegawai->num_rows == 0) {
                        echo '<option>Tidak Ada</option>';
                    } else {
                        ?>
                            <label class="col-12 col-form-label"><strong><?php echo strtoupper($row->JABATAN);?></strong></label>
                            <div class="input-group">
                                <div class="col-md-12">
                        <?php
                        while ($data = $pegawai->fetch_object()) {  // MENAMPILKAN DATA PEGAWAI YANG DIDAPAT
                            $status = "";
                            if (isset($_GET['kode_jadwal'])) {
                                if (in_array($data->NIP, $list_petugas)) {
                                    $status = "checked";
                                } else{
                                    $TANGGAL = array(
                                        'MULAI' => $_GET['tanggal_mulai'],
                                        'SELESAI' => $_GET['tanggal_selesai']
                                    );
                                    $checkDuty = $pgw->status_penugasan($data->NIP, $TANGGAL);
                                    $status = ($checkDuty->num_rows != 0) ? 'disabled' : '';  // MENGECEK KETERSEDIAAN PEGAWAI
                                }
                            }
                            ?>
                                <div class="col-md-4" style="float: left;">
                                    <div class="input-group-text">
                                        <input type="checkbox" name="pegawai[<?php echo $no;?>]" id="pegawai[<?php echo $no;?>]" value="<?php echo $data->NIP;?>" class="filled-in chk-col-cyan" <?php echo $status;?>>
                                        <label for="pegawai[<?php echo $no;?>]" class="mb-0"><?php echo ucfirst($data->NAMA);?></label>
                                    </div>
                                </div>
                            <?php
                            $no++;
                        }
                        ?>
                                <input type="hidden" name="TOTAL_PEGAWAI" value="<?php echo $no;?>"/>
                                </div>
                            </div>
                            <hr>
                        <?php
                    }        
                }
            }
        ?>
    </div>
    <content>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="../assets/plugins/bootstrap/js/tether.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="../assets/plugins/bootstrap/js/multi-select.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="../assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="../js/custom.min.js"></script>
</body>

</html>
